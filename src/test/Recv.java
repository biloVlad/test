/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import org.bson.Document;

/**
 *
 * @author StoneInside
 */
public class Recv {
     private final static String QUEUE_NAME = "hello";
    private final static String NAME_DATABASE = "db";
    private final static String NAME_COLLECTION = "testMQ";

    public static void main(String[] argv) throws Exception {
        System.out.println("Connecting to rabbitMQ...");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        
        System.out.println("Connecting to mongoDB...");
        MongoClient mongoClient = new MongoClient();
        MongoDatabase database = mongoClient.getDatabase(NAME_DATABASE);
        MongoCollection<Document> collection = database.getCollection(NAME_COLLECTION);

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for message. To exit press CTRL+C");
        
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");

                Document doc = new Document("type", "getMQ")
                                    .append("value", message);
                collection.insertOne(doc);

                System.out.println(" [x] Received '" + message + "'");
                System.out.println("Inserted in " + NAME_DATABASE + "." + NAME_COLLECTION + ":");
                System.out.println(doc);
                System.out.println();
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
