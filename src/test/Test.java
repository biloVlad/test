/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClients;
//import com.mongodb.client.MongoClient;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import com.mongodb.ServerAddress;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;

/**
 *
 * @author StoneInside
 */
import java.util.concurrent.TimeoutException;
import org.bson.Document;
public class Test {      
    public static void main(String[] argv) throws java.io.IOException, TimeoutException {
        MongoClient mongoClient = new MongoClient();
        
        MongoDatabase database = mongoClient.getDatabase("db");
        //System.out.println(database);
        
        MongoCollection<Document> collection = database.getCollection("test");
        //System.err.println(collection);
        
        Document doc = new Document("name", "MongoDB")
                        .append("type", "database");
        collection.insertOne(doc);
        System.out.println(doc);
        
    }
}
